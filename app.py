from darkflow.net.build import TFNet
import cv2
import numpy as np
import sys
import requests
import json
import time


class PeopleCounter:
    def __init__(self, work_type):
        self.net_options = {"pbLoad": "./darkflow/tiny-yolo-voc-2c.pb", "metaLoad": "./darkflow/tiny-yolo-voc-2c.meta", "threshold": 0.5}
        self.tfnet = TFNet(self.net_options)
        if int(work_type) == 1:
            self.cap = cv2.VideoCapture("./1.mp4")
        elif int(work_type) == 2:
            self.cap = cv2.VideoCapture(0)
        self.optical_count = 0
        self.in_count = 0
        self.out_count = 0
        self.alias = "demo"
        self.api_url = "http://graduate.tolgabilbey.com/api/" + self.alias + "/"
        self.token = "Token 97975f54c603994d7def8d084c694392fc806037"
        start_time = time.time()
        self.read_video()
        print(time.time() - start_time)
        print(self.cap.get(cv2.CAP_PROP_FPS))
        
        
    def read_video(self):
        
        while(1):
            ret, frame = self.cap.read()
            
            if ret:
                bboxes = self.tfnet.return_predict(frame)
                self.draw_bounding(frame, bboxes)
                self.door = self.get_door(bboxes)
                self.persons = self.get_points(bboxes)
                if self.door is not None and self.persons is not None:
                    check = self.optical_check()
                    if check:
                        self.optical(frame)
            else:
                break

    def draw_bounding(self, frame, bboxes):
        show_frame = frame.copy()
        for box in bboxes:
            x1 = box["bottomright"]["x"]
            x2 = box["topleft"]["x"]
            y1 = box["bottomright"]["y"]
            y2 = box["topleft"]["y"]
            if box["label"] == "person":
                cv2.rectangle(show_frame, (x1, y1), (x2, y2), (0,255,0), 2)
                cv2.putText(show_frame, "person", (x2, y2+20), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255,255,255), 2)
            else:
                cv2.rectangle(show_frame, (x1, y1), (x2, y2), (0,0,255), 2)
                cv2.putText(show_frame, "door", (x2, y2+20), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255,255,255), 2)
        self.show_image(show_frame)
        
    def get_points(self, bboxes):
        points = list(list())
        for box in bboxes:
            if box["label"] == "person" and len(points) < 1:
                x1 = box["bottomright"]["x"]
                x2 = box["topleft"]["x"]
                y1 = box["bottomright"]["y"]
                y2 = box["topleft"]["y"]
                points.append(np.array([(x1+x2)/2,(y1+y2)/2], dtype="f").reshape(1,2))
        np_points = np.array(points, dtype="f")
        return np_points

    def get_door(self, boxes):
        for box in boxes:
            if box["label"] == "door":
                x1 = box["bottomright"]["x"]
                x2 = box["topleft"]["x"]
                y1 = box["bottomright"]["y"]
                y2 = box["topleft"]["y"] + 100
                return [[x1,y1], [x2,y2]]

    def optical_check(self):
        for person in self.persons:
            if self.door[0][0] > person[0][0] and self.door[1][0] < person[0][0] and self.door[0][1] > person[0][1] and self.door[1][1] < person[0][1]:
                return True

    def write_counts(self, frame, text, type):
        if type == 1:
            x = 900
            y = 40
        elif type == 2:
            x = 900
            y = 70
        cv2.putText(frame, text, (x, y), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255,255,255), 2)

    def show_image(self, frame):
        show_frame = frame.copy()
        self.write_counts(show_frame, "In: " + str(self.in_count), 1)
        self.write_counts(show_frame, "Out: " + str(self.out_count), 2)
        cv2.imshow("window", show_frame)
        cv2.waitKey(1000)

    def optical(self, img):
        self.optical_count += 1
        old_gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        p0 = self.persons
        entry = p0
        color = np.random.randint(0,255,(100,3))
        mask = np.zeros_like(img)
        directions = []
        check = False
        while(1):
            ret, frame = self.cap.read()
            showing_img = frame.copy()
            bboxes = self.tfnet.return_predict(showing_img)
            frame_gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
            p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None)
            
            if p1[0][0][1] - p0[0][0][1] > 0:
                direction = "down"
            elif p1[0][0][1] - p0[0][0][1] < 0:
                direction = "up"
            else:
                direction = "stay"
            directions.append(direction)

            
            if (p1[0][0][1] - self.door[0][1] > 0) or (p1[0][0][1] - self.door[1][1] < 0):
                if (p0[0][0][1] - self.door[0][1] > 0) or (p0[0][0][1] - self.door[1][1] < 0):
                    check = False
                else:
                    check = True
            else:
                check = False
        
            if not [1] in st and check and len(directions) > 1 and err < 20:
                up_count = directions.count("up")
                down_count = directions.count("down")

                if up_count > down_count:
                    self.in_count += 1
                    self.send_counts()
                else:
                    self.out_count += 1
                    self.send_counts()
                print("in: ", self.in_count)
                print("out: ", self.out_count)
                break

            elif not [1] in st and not check:
                break

            elif err > 10:
                if len(directions) > 14:
                    up_count = directions.count("up")
                    down_count = directions.count("down")

                    if up_count > down_count:
                        self.in_count += 1
                        self.send_counts()

                    elif down_count > up_count:
                        self.out_count += 1
                        self.send_counts()
                    print("in: ", self.in_count)
                    print("out: ", self.out_count)
                    break
                else:
                    break

            good_new = p1[st==1]
            good_old = p0[st==1]

            for i,(new,old) in enumerate(zip(good_new,good_old)):
                a,b = new.ravel()
                c,d = old.ravel()
                mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
                showing_img = cv2.circle(showing_img,(a,b),5,color[i].tolist(),-1)
            img = cv2.add(showing_img,mask)
            self.show_image(img)

            old_gray = frame_gray.copy()
            p0 = good_new.reshape(-1,1,2)
        cv2.destroyAllWindows()
        self.read_video()

    def send_counts(self):
        headers = {
            "Authorization": self.token
        }
        data = {
            "in_count": self.in_count,
            "total_count": self.in_count - self.out_count
        }
        res = requests.request("PATCH", self.api_url, headers=headers, data=data)

if __name__ == '__main__':
    counter = PeopleCounter(sys.argv[1])